from models.bar_model import get_model
import keras
from keras.callbacks import LearningRateScheduler

from sklearn.model_selection import train_test_split

from src import midi_converter, dbnn_transposer, bars

from os import listdir
from os.path import isfile, join



def learning_rate(epoch):
    lrate = 0.009 * pow(0.95, epoch)
    print("lr :" + str(lrate))
    print("")
    return lrate




mc = keras.callbacks.ModelCheckpoint('./bar_model_weights/bach_note.h5', save_weights_only=True, period=10)


folder = "./midi/bach"
files = [join(folder, f) for f in listdir(folder) if isfile(join(folder, f))]

sequence_length = 16

X = []
Y = []

for file in files:
    dbnns = midi_converter.file_to_dbnn(file)
    if dbnns == None:
        continue
    for dbnn_pre in dbnns:
        transposed = dbnn_transposer.transpose_in_range(dbnn_pre, 5, 5)
        for dbnn in transposed:
            note_bars = bars.divide_dbnn_into_bars(dbnn)
            bar_vectors = bars.bar_notes_to_bars(note_bars, dbnn)

            for ind in range(sequence_length, len(bar_vectors)):
                X.append(bar_vectors[ind - sequence_length: ind])
                Y.append(bar_vectors[ind])

x_train, x_test, y_train, y_test = train_test_split(X, Y, test_size=0.1, shuffle= True)


lrate = LearningRateScheduler(learning_rate)
callbacks = [mc, lrate]

m = get_model(0.009, False)
history = m.fit(x_train, y_train,validation_data=(x_test, y_test), callbacks = callbacks, batch_size = 32, epochs = 1000)



