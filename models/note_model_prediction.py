from src import dbnn_converter

from models.note_model import get_prediction_model
import numpy as np


def predict_next(model, X):
    model.reset_states()
    prediction = model.predict({'input_notes' : np.array([X['n']]),
                                'input_lenghts' : np.array([X['l']]),
                                'input_delays': np.array([X['d']]),
                                'input_velo': np.array([X['v']]),
                                })

    kokku = {
        'n' : prediction[0][0],
        'l' : prediction[1][0],
        'd' : prediction[2][0],
        'v' : prediction[3][0]
    }

    return kokku


def predict(ammount, X, model, extend=True, max_len = 300):
    newSong = {
        'n' : [],
        'd' : [],
        'l' : [],
        'v' : []
    }

    seed = X

    if extend:
        newSong['n'].extend(seed['n'])
        newSong['l'].extend(seed['l'])
        newSong['d'].extend(seed['d'])
        newSong['v'].extend(seed['v'])

    for x in range(0, ammount):
        new_note = predict_next(model, seed)

        newSong['n'].append(new_note['n'])
        newSong['l'].append(new_note['l'])
        newSong['d'].append(new_note['d'])
        newSong['v'].append(new_note['v'])

        seed = newSong.copy()
        print()
        if len(seed['n']) > max_len:
            seed['n'] = seed['n'][-max_len - 1:]
            seed['l'] = seed['l'][-max_len - 1:]
            seed['d'] = seed['d'][-max_len - 1:]
            seed['v'] = seed['v'][-max_len - 1:]

        print(len(newSong['n']), "/", (ammount + max_len), end = "\r")


    return newSong


def generate(input_sequence_length, weights_file, seed_dbnn, extend=False, amount=100,
                 temperature=0.5, bar_vecs=False, tempo=600000):

    mdl = get_prediction_model()
    mdl.load_weights(weights_file)

    dbnn = seed_dbnn
    seq_length = input_sequence_length

    v_list_dict = dbnn_converter.dbnn_to_vecs_dict(dbnn)
    v_list_dict['n'] = v_list_dict['n'][0:seq_length]
    v_list_dict['l'] = v_list_dict['l'][0:seq_length]
    v_list_dict['d'] = v_list_dict['d'][0:seq_length]
    v_list_dict['v'] = v_list_dict['v'][0:seq_length]

    song = dbnn_converter.vecs_dict_to_dbnn(predict(amount, v_list_dict, mdl, True, max_len = seq_length), 0.5)
    song.timing.tempo = tempo

    if not extend:
        song.notes = song.notes[seq_length:]

    return song





'''
folder_train = "./midi/romantic_val"
files_train = [join(folder_train, f) for f in listdir(folder_train) if isfile(join(folder_train, f))]
X_n_train, X_l_train, X_d_train, X_v_train, Y_n_train, Y_l_train, Y_d_train, Y_v_train = data.load_data(files_train, transpose = (0,1))

result = mdl.evaluate(    { 'input_notes' : np.array(X_n_train),
      'input_lenghts' : np.array(X_l_train),
      'input_delays' : np.array(X_d_train),
       'input_velo': np.array(X_v_train)
      },
    [np.array(Y_n_train), np.array(Y_l_train), np.array(Y_d_train), np.array(Y_v_train)])

for i, metric in enumerate(mdl.metrics_names):
    print(metric, ": ", round(result[i], 3))

exit()

'''