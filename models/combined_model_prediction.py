from src import bars, dbnn_converter
from src.note_durations import duration_lengths

from models.combined_model import get_prediction_model
import numpy as np

from src.combined_model_data import match_bars_to_notes


def predict_next(model, X):
    #model.reset_states()
    prediction = model.predict({'input_notes' : np.array([X['n']]),
                                'input_lenghts' : np.array([X['l']]),
                                'input_delays': np.array([X['d']]),
                                'input_velo': np.array([X['v']]),
                                'input_bar': np.array([X['b']])
                                })

    kokku = {
        'n' : prediction[0][0],
        'l' : prediction[1][0],
        'd' : prediction[2][0],
        'v' : prediction[3][0]
    }

    return kokku


def predict(ammount, X, model, extend=True, bar_vecs = None, bar_length = 1, max_len = 100):
    cur_bar_length = 0
    bar_index = 0

    seed_len = len(X['n'])


    newSong = {
        'n' : [],
        'd' : [],
        'l' : [],
        'v' : [],
        'b' : []
    }

    seed = X

    if extend:
        newSong['n'].extend(seed['n'])
        newSong['l'].extend(seed['l'])
        newSong['d'].extend(seed['d'])
        newSong['v'].extend(seed['v'])
        newSong['b'].extend(seed['b'])

    for x in range(0, ammount):
        new_note = predict_next(model, seed)

        newSong['n'].append(new_note['n'])
        newSong['l'].append(new_note['l'])
        newSong['d'].append(new_note['d'])
        newSong['v'].append(new_note['v'])
        newSong['b'].append(bar_vecs[bar_index])

        seed = newSong.copy()

        if len(seed['n']) > max_len:
            seed['n'] = seed['n'][0-max_len-1:]
            seed['l'] = seed['l'][0-max_len-1:]
            seed['d'] = seed['d'][0-max_len-1:]
            seed['v'] = seed['v'][0-max_len-1:]
            seed['b'] = seed['b'][0-max_len-1:]

        print(len(newSong['n']), "/", ammount + seed_len, end = '\r')

        cur_bar_length += duration_lengths[dbnn_converter.vec_to_duration(new_note['d'])] 

        if cur_bar_length >= bar_length:
            cur_bar_length = cur_bar_length - bar_length
            bar_index += 1

    return newSong

def generate(input_sequence_length, weights_file, seed_dbnn, extend = False, amount = 100, bar_length = 1, temperature = 0.5, bar_vecs = False, tempo = 500000):
    seq_length = input_sequence_length
    mdl = get_prediction_model(cpu_training = True)
    mdl.load_weights(weights_file)

    dbnn = seed_dbnn

    v_list_dict = dbnn_converter.dbnn_to_vecs_dict(dbnn)
    new_bars = match_bars_to_notes(dbnn)

    v_list_dict['n'] = v_list_dict['n'][0:seq_length]
    v_list_dict['l'] = v_list_dict['l'][0:seq_length]
    v_list_dict['d'] = v_list_dict['d'][0:seq_length]
    v_list_dict['v'] = v_list_dict['v'][0:seq_length]
    v_list_dict['b'] = new_bars[0:seq_length]

    if bar_vecs == False:
        prediction_bars = bars.bar_notes_to_bars(bars.divide_dbnn_into_bars(dbnn), dbnn)
    else:
        prediction_bars = bar_vecs

    bar_length = bar_length * (dbnn.timing.numerator / dbnn.timing.denominator)
    song = dbnn_converter.vecs_dict_to_dbnn(predict(amount, v_list_dict, mdl, True, bar_vecs = prediction_bars, bar_length = bar_length, max_len=seq_length), temperature)
    song.timing.tempo = tempo

    if not extend:
        song.notes = song.notes[seq_length:]

    return song


'''
folder_train = "./midi/classic_val"
files_train = [join(folder_train, f) for f in listdir(folder_train) if isfile(join(folder_train, f))]
X_n_train, X_l_train, X_d_train, X_v_train, X_b_train, Y_n_train, Y_l_train, Y_d_train, Y_v_train = data.load_data(files_train, transpose = (0,1))

result = mdl.evaluate(    { 'input_notes' : np.array(X_n_train),
      'input_lenghts' : np.array(X_l_train),
      'input_delays' : np.array(X_d_train),
       'input_velo': np.array(X_v_train),
       'input_bar': np.array(X_b_train)
      },
    [np.array(Y_n_train), np.array(Y_l_train), np.array(Y_d_train), np.array(Y_v_train)])
print(result)

for i, metric in enumerate(mdl.metrics_names):
    print(metric, ": ", round(result[i], 3))

exit()

'''


'''
for i in range(10):
    prediction_bars.append(prediction_bars[i])
prediction_bars = []
for i in range(50):
    starter = [0,0,1,0,0,0,1,0,0,1,0,0]
    prediction_bars.append(starter)
    starter = [0,1,0,1,1,0,1,0,1,1,0,0]
    prediction_bars.append(starter)
    starter = [0,0,1,0,0,1,0,0,0,0,0,1]
    prediction_bars.append(starter)
    starter = [0,0,1,0,0,0,1,0,0,1,0,0]
    prediction_bars.append(starter)
    starter = [0,0,1,0,0,0,0,1,0,0,0,1]
    prediction_bars.append(starter)
    starter = [0,0,1,0,0,0,1,0,0,1,0,0]
    prediction_bars.append(starter)
    starter = [0,0,1,0,0,0,0,1,0,0,0,1]
    prediction_bars.append(starter)
    starter = [0,1,0,0,1,0,0,0,0,1,0,0]
'''


'''
for note in song.notes:
    note.velocity = 100
'''



