
import numpy as np
from keras.layers import LSTM
from keras.layers import CuDNNLSTM
from keras.layers import CuDNNGRU
from keras.layers import Input
from keras.layers import Dense
from keras.layers import Dropout
from keras.layers import concatenate
from keras.models import Model
from keras import Sequential
from keras.layers import TimeDistributed
from keras.optimizers import RMSprop
import functools
from keras.optimizers import Adam
import keras



def top3_categorical_acc(y_true, y_pred):
    return keras.metrics.top_k_categorical_accuracy(y_true, y_pred, k=3)


def get_model(lr = 0.01, cpu_training = False, batch_shape = (32, 20, 113)):
    optimizer = RMSprop(lr = lr)
    #optimizer = Adam(lr)

    if cpu_training:
        RNNLayer = keras.layers.LSTM
    else:
        RNNLayer = keras.layers.CuDNNLSTM
    

    #dropout ratio
    drop_r = 0.2

    #fix: +125
    #input_all = Input(shape = (None, 113), batch_shape = batch_shape)

    input_velocity = Input(shape = (None), name='input_velo', batch_shape = (batch_shape[0], batch_shape[1], 1))
    input_notes = Input(shape = (None, 88), name = 'input_notes', batch_shape = (batch_shape[0], batch_shape[1], 88))
    input_durations = Input(shape = (None, 12), name= 'input_lenghts', batch_shape = (batch_shape[0], batch_shape[1], 12))
    input_delays = Input(shape = (None, 12), name = 'input_delays', batch_shape = (batch_shape[0], batch_shape[1], 12))
    input_bar = Input(shape = (None, 12), name = 'input_bar', batch_shape = (batch_shape[0], batch_shape[1], 12))

    lstm_pre_velo = RNNLayer(8, return_sequences = True, stateful = True)(input_velocity)
    lstm_pre_notes = RNNLayer(64, return_sequences = True, stateful = True)(input_notes)
    lstm_pre_durations = RNNLayer(32, return_sequences = True, stateful = True)(input_durations)
    lstm_pre_delays = RNNLayer(32, return_sequences = True, stateful = True)(input_delays)
    lstm_pre_bar = RNNLayer(32, return_sequences = True, stateful = True)(input_bar)

    concat_layer = concatenate([lstm_pre_velo, lstm_pre_notes, lstm_pre_durations, lstm_pre_delays, lstm_pre_bar])

    c_lstm = RNNLayer(128, return_sequences = True, stateful = True)(concat_layer)
    dropout_1 = Dropout(drop_r)(c_lstm)

    lstm_notes = RNNLayer(64, return_sequences=False, stateful = True)(dropout_1)
    lstm_lengths = RNNLayer(32, return_sequences=False, stateful = True)(dropout_1)
    lstm_delays = RNNLayer(32, return_sequences=False, stateful = True)(dropout_1)
    lstm_velo = RNNLayer(8, return_sequences=False, stateful = True)(dropout_1)

    #notes
    dropout_notes = Dropout(drop_r)(lstm_notes)
    output_notes = Dense(88, activation='softmax', name = 'output_notes')(dropout_notes)

    #note lenghts
    dropout_lengths = Dropout(drop_r)(lstm_lengths)
    output_lengths = Dense(12 , activation='softmax', name = 'output_lengths')(dropout_lengths)

    #note delays
    dropout_delays = Dropout(drop_r)(lstm_delays)
    output_delays = Dense(12, activation='softmax', name = 'output_delays')(dropout_delays)

    #velocity
    dropout_velo = Dropout(drop_r)(lstm_velo)
    output_velo = Dense(1, activation='sigmoid', name = 'output_velo')(dropout_velo)

    model = Model(inputs=[input_velocity, input_notes, input_durations, input_delays, input_bar], outputs=[output_notes, output_lengths, output_delays, output_velo])

    model.compile(
        loss={
            'output_notes' : 'categorical_crossentropy',
            'output_lengths' : 'categorical_crossentropy',
            'output_delays' : 'categorical_crossentropy',
            'output_velo' : 'mse'
        },
        loss_weights = {
            'output_notes' : 0.5,
            'output_lengths' : 0.2,
            'output_delays' : 0.2,
            'output_velo' : 0.05
        },
        optimizer=optimizer,
        metrics={
            "output_notes": [ keras.metrics.categorical_accuracy, top3_categorical_acc ],
            "output_lengths": keras.metrics.categorical_accuracy,
            'output_delays': keras.metrics.categorical_accuracy,
            'output_velo': keras.metrics.mean_absolute_error
        }
        )
    return model


def get_prediction_model(lr = 0.01, cpu_training = True):
    optimizer = RMSprop(lr=lr)
    # optimizer = Adam(lr)

    if cpu_training:
        RNNLayer = keras.layers.LSTM
    else:
        RNNLayer = keras.layers.CuDNNLSTM

    # dropout ratio
    drop_r = 0.2

    # fix: +125
    # input_all = Input(shape = (None, 113), batch_shape = batch_shape)

    input_velocity = Input(shape=(None, 1), name='input_velo')
    input_notes = Input(shape=(None, 88), name='input_notes')
    input_durations = Input(shape=(None, 12), name='input_lenghts')
    input_delays = Input(shape=(None, 12), name='input_delays')
    input_bar = Input(shape = (None, 12), name = 'input_bar')

    lstm_pre_velo = RNNLayer(8, return_sequences=True, stateful=False)(input_velocity)
    lstm_pre_notes = RNNLayer(64, return_sequences=True, stateful=False)(input_notes)
    lstm_pre_durations = RNNLayer(32, return_sequences=True, stateful=False)(input_durations)
    lstm_pre_delays = RNNLayer(32, return_sequences=True, stateful=False)(input_delays)
    lstm_pre_bar = RNNLayer(32, return_sequences = True, stateful = False)(input_bar)

    concat_layer = concatenate([lstm_pre_velo, lstm_pre_notes, lstm_pre_durations, lstm_pre_delays, lstm_pre_bar])

    c_lstm = RNNLayer(256, return_sequences=True, stateful=False)(concat_layer)
    dropout_1 = Dropout(drop_r)(c_lstm)

    lstm_notes = RNNLayer(64, return_sequences=False, stateful=False)(dropout_1)
    lstm_lengths = RNNLayer(32, return_sequences=False, stateful=False)(dropout_1)
    lstm_delays = RNNLayer(32, return_sequences=False, stateful=False)(dropout_1)
    lstm_velo = RNNLayer(8, return_sequences=False, stateful=False)(dropout_1)

    # notes
    dropout_notes = Dropout(drop_r)(lstm_notes)
    output_notes = Dense(88, activation='softmax', name='output_notes')(dropout_notes)

    # note lenghts
    dropout_lengths = Dropout(drop_r)(lstm_lengths)
    output_lengths = Dense(12, activation='softmax', name='output_lengths')(dropout_lengths)

    # note delays
    dropout_delays = Dropout(drop_r)(lstm_delays)
    output_delays = Dense(12, activation='softmax', name='output_delays')(dropout_delays)

    # velocity
    dropout_velo = Dropout(drop_r)(lstm_velo)
    output_velo = Dense(1, activation='sigmoid', name='output_velo')(dropout_velo)

    model = Model(inputs=[input_velocity, input_notes, input_durations, input_delays, input_bar],
                  outputs=[output_notes, output_lengths, output_delays, output_velo])

    model.compile(
        loss={
            'output_notes': 'categorical_crossentropy',
            'output_lengths': 'categorical_crossentropy',
            'output_delays': 'categorical_crossentropy',
            'output_velo': 'mse'
        },
        loss_weights={
            'output_notes': 0.5,
            'output_lengths': 0.2,
            'output_delays': 0.2,
            'output_velo': 0.05
        },
        optimizer=optimizer,
        metrics={
            "output_notes": [keras.metrics.categorical_accuracy, top3_categorical_acc],
            "output_lengths": keras.metrics.categorical_accuracy,
            'output_delays': keras.metrics.categorical_accuracy,
            'output_velo': keras.metrics.mean_absolute_error
        }
    )
    return model

