from models.bar_model import get_model

from src import bars
import numpy as np


def generate(ammount, weights_file, dbnn):
    newBars = []
    model = get_model(cpu_training=True);
    model.load_weights(weights_file)

    bar_notes = bars.divide_dbnn_into_bars(dbnn)
    bar_vecs = bars.bar_notes_to_bars(bar_notes, dbnn)


    seed = bar_vecs[-16:]
    seq_length = len(seed)

    for x in range(0, ammount):
        new_bar = model.predict(np.array([seed]))


        l = [x for x in new_bar[0]]
        s = sorted(l)
        new_bar[0] = np.exp(new_bar[0] * 5) / np.max(np.exp(new_bar[0] * 5))
        new_bar[0] = [0 if x < s[6] else x for x in new_bar[0]]



        newBars.append(new_bar)
        seed = np.array(seed)

        seed = np.append(seed[1:], new_bar)
        seed = np.reshape(seed, (seq_length, 12))

    for i, bar in enumerate(newBars):
        newBars[i] = bar[0]

    return newBars

'''
from midi_output import MidiOutput
mo = MidiOutput(0)
mid = mido.MidiFile()
track = mido.MidiTrack()
mid.tracks.append(track)

dbnn = midi_converter.file_to_dbnn('./midi/v/apologize.mid')
predictions = generate(12, './bar_model_weights/beethoven.h5', dbnn)





for bar in predictions:
    first = True
    for i, b in enumerate(bar):
        if (b > 0.02):
            if first:
                track.append(mido.Message(type="note_on", note=60 + i, velocity=int(80 * b), time=0))
            else:
                track.append(mido.Message(type="note_on", note=60 + i, velocity=int(80 * b), time=0))

            first = False

    first = True
    for i, b in enumerate(bar):
        if (b > 0.02):
            if first:
                track.append(mido.Message(type="note_off", note=60 + i, velocity=80, time=1000))
            else:
                track.append(mido.Message(type="note_off", note=60 + i, velocity=80, time=0))

            first = False

for msg in mid.play():
    print(msg)
    mo.send_to_output(msg)
'''