
import numpy as np
from keras.layers import LSTM
from keras.layers import CuDNNLSTM
from keras.layers import Input
from keras.layers import Dense
from keras.layers import Dropout
from keras.models import Model
from keras import Sequential
from keras.layers import TimeDistributed
from keras.optimizers import RMSprop
from keras.optimizers import Adam
import keras
import keras.backend as K
import tensorflow as tf

def top_3_same_acc(y_true, y_pred):
    true_top = tf.contrib.framework.sort(tf.math.top_k(y_true, k = 3, sorted = False).indices)
    pred_top = tf.contrib.framework.sort(tf.math.top_k(y_pred, k = 3, sorted = False).indices)
    return K.cast(K.equal(true_top, pred_top), dtype=K.floatx())

def get_model(lr = 0.01, cpu_training = False):
    #optimizer = RMSprop(lr = lr)
    optimizer = Adam(lr)

    if cpu_training:
        RNNLayer = keras.layers.LSTM
    else:
        RNNLayer = keras.layers.CuDNNLSTM

    '''
    input_velocity = Input(shape = (None, 1))
    input_notes = Input(shape = (None, 88)) 
    input_durations = Input(shape = (None, 12))
    input_delays = Input(shape = (None, 12)) 
    '''

    input_all = Input(shape = (None, 12))
    lstm_1 = RNNLayer(64, return_sequences=True)(input_all)
    lstm2 = RNNLayer(32, return_sequences=False)(lstm_1)

    output = Dense(12, activation = "sigmoid")(lstm2)

    model = Model(inputs=input_all, outputs=output)

    model.compile(
        loss='binary_crossentropy',
        optimizer=optimizer,
        metrics=['mean_squared_error', top_3_same_acc],
        )
    return model


