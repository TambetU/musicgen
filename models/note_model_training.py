from models.note_model import get_model
import numpy as np
import keras
from keras.callbacks import LearningRateScheduler

from src import note_model_data as data

from os import listdir
from os.path import isfile, join


def learning_rate(epoch):
    lrate = 0.001 * pow(0.95, epoch)
    print("lr :" + str(lrate))
    print("")
    return lrate

class StateReseting(keras.callbacks.Callback):
    def on_batch_begin(self, batch, logs=None):
        if batch % 1 == 0:
            self.model.reset_states()


mc = keras.callbacks.ModelCheckpoint('./weights/bach_note{epoch:05d}.h5', save_weights_only=True, period=1)

m = get_model(0.001, False, batch_shape=(32, 30, 50))

#m.load_weights('./a_model_weights/v_m_weights00005.h5')


folder_train = "./midi/bach"
folder_test = "./midi/bach_val"
tensorCb = keras.callbacks.TensorBoard(log_dir='./logs/bach_note', histogram_freq=0, batch_size=32, write_graph=True, write_grads=False, write_images=False, embeddings_freq=0, embeddings_layer_names=None, embeddings_metadata=None)

files_train = [join(folder_train, f) for f in listdir(folder_train) if isfile(join(folder_train, f))]
files_test = [join(folder_test, f) for f in listdir(folder_test) if isfile(join(folder_test, f))]

X_n_train, X_l_train, X_d_train, X_v_train, Y_n_train, Y_l_train, Y_d_train, Y_v_train = data.load_data(files_train, sequence_length = 30, samples_max = 1, transpose = (0,0))
X_n_test, X_l_test, X_d_test, X_v_test, Y_n_test, Y_l_test, Y_d_test, Y_v_test = data.load_data(files_test,sequence_length = 30,samples_max = 1, transpose = (0,0))

lrate = LearningRateScheduler(learning_rate)
callbacks = [mc, tensorCb, StateReseting(), lrate]


history = m.fit(
    {'input_notes': np.array(X_n_train),
     'input_lenghts': np.array(X_l_train),
     'input_delays': np.array(X_d_train),
     'input_velo': np.array(X_v_train)
     },
    [np.array(Y_n_train), np.array(Y_l_train), np.array(Y_d_train), np.array(Y_v_train)],
    validation_data=(
        {'input_notes': np.array(X_n_test),
         'input_lenghts': np.array(X_l_test),
         'input_delays': np.array(X_d_test),
         'input_velo': np.array(X_v_test)
         },
        [np.array(Y_n_test), np.array(Y_l_test), np.array(Y_d_test), np.array(Y_v_test)]),
    verbose = 2,
    callbacks = callbacks,
    batch_size = 32,
    epochs = 1000,
    shuffle = False
)


