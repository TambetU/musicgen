import sys
sys.path.append("..")

from src import midi_converter, bars
from models import combined_model_prediction, bar_prediction
import tensorflow as tf
import keras.backend as K

num_CPU = 1
num_GPU = 0
num_cores = 4

config = tf.ConfigProto(
    intra_op_parallelism_threads=num_cores, \
    inter_op_parallelism_threads=num_cores, allow_soft_placement=True, \
    device_count={'CPU': num_CPU, 'GPU': num_GPU})

session = tf.Session(config=config)
K.set_session(session)

dbnn = midi_converter.file_to_dbnn('./../midi/all_val/mond_1.mid')[0]
note_bars = bars.divide_dbnn_into_bars(dbnn)

start = 6
end = 12
note_bars = note_bars[start:end]

note_counter = 0
for i, n in enumerate(note_bars):
    for l in n:
        note_counter+= 1
    if i == end-1:
        break

dbnn.notes = dbnn.notes[:note_counter]
bar_vecs = bars.bar_notes_to_bars(note_bars, dbnn)
bs = bar_prediction.generate(40, './../bar_model_weights/schubert.h5', dbnn)

'''
bs = [
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
]
'''

dbnn_gener = combined_model_prediction.generate(50, "./../weights/schubert_combined.h5", dbnn, bar_vecs=bs, bar_length=1,
                                                amount=100, temperature=1, extend=False)
note_bars_gen = bars.divide_dbnn_into_bars(dbnn_gener)


import matplotlib.pyplot as plt
plt.rcParams["figure.figsize"] = [12,4]
fig, ax = plt.subplots()

note_bars_flat= []
for x in note_bars_gen:
    for a in x:
        note_bars_flat.append(a)

y_lim_min = min(note_bars_flat, key=lambda x: x[2])[2] - 21 - 1
y_lim_max = max(note_bars_flat, key=lambda x: x[2])[2] + 1 - 21


from src.midi_utils import note_names
labels = []
y_ticks = []

for a in range(y_lim_min, y_lim_max):
    labels.append(note_names[a % 12] + str(a // 12 + 1))
    y_ticks.append(a)

labels = [l if i % 2 == 0 else "" for i, l in enumerate(labels)]


#värv #ff990d
plt.plot([6, 6], [0, 100], lw=1, ls = "--", color = "black")


for n in range(88):
    ax.broken_barh([(ind, 0.9) for ind, x in enumerate(bar_vecs)], (n-0.25, 0.5),
                   facecolors=[(1,0.6,0.05, min(1, x[(n-3) % 12])) for ind, x in enumerate(bar_vecs)])

for n in range(88):
    ax.broken_barh([(ind + (end-start), 0.9) for ind, x in enumerate(bs)], (n-0.25, 0.5),
                   facecolors=[(0.1,0.6,0.5, min(1, x[(n - 3) % 12])) for ind, x in enumerate(bs)])

for i, bar in enumerate(note_bars):
    for note in bar:
        ax.broken_barh([(i + note[0], note[1])], (note[2]- 21 - 0.25, 0.5), facecolors = 'black')

for i, bar in enumerate(note_bars_gen):
    for note in bar:
        ax.broken_barh([(i + end- start + note[0], note[1])], (note[2]-  21 -0.25, 0.5), facecolors = 'black')


ax.set_yticklabels(labels)
ax.set_yticks(y_ticks)
ax.set_ylabel('Noot')
ax.set_xlabel('Takt')
ax.set_ylim(y_lim_min +4 ,y_lim_max-3)
ax.set_xlim(3, 12)

plt.show()