import sys
sys.path.append("..")

from os import listdir
from os.path import isfile, join
import numpy as np
from src import midi_converter, bars

from models import bar_model

mdl = bar_model.get_model(0.01, True)


datasets = ["chopin", "beethoven", "schubert", "classic", "romantic", "all"]


for d in datasets:
    folder_test = "./../midi/" + d + "_val"
    files_test = [join(folder_test, f) for f in listdir(folder_test) if isfile(join(folder_test, f))]
    sequence_length = 16
    X = []
    Y = []
    mdl.load_weights("./../bar_model_weights/" + d + ".h5")
    for f in files_test:

        dbnns = midi_converter.file_to_dbnn(f)

        for dbnn in dbnns:
            note_bars = bars.divide_dbnn_into_bars(dbnn)
            bar_vectors = bars.bar_notes_to_bars(note_bars, dbnn)

            for ind in range(sequence_length, len(bar_vectors)):
                X.append(bar_vectors[ind - sequence_length: ind])
                Y.append(bar_vectors[ind])

    results = mdl.evaluate(np.array(X), np.array(Y), verbose=0)
    print(results[2])