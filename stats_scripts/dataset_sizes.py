from os import listdir as listdir
from os.path import join
from os.path import isfile

import sys
sys.path.append("..")
from src import midi_converter as md, combined_model_data as data

datasets = ["chopin", "beethoven", "schubert", "classic", "romantic", "all"]

datasets = ["bach"]
dct = {}
for d in datasets:
    folder_train = "./../midi/" + d
    folder_test = "./../midi/" + d + "_val"

    dct[folder_train] = 0
    dct[folder_test] = 0

    files_train = [join(folder_train, f) for f in listdir(folder_train) if isfile(join(folder_train, f))]
    files_test = [join(folder_test, f) for f in listdir(folder_test) if isfile(join(folder_test, f))]

    for file in files_train:
        dbnns = md.file_to_dbnn(file)

        if dbnns == None:
           continue

        for dbnn in dbnns:
            dct[folder_train] += len(dbnn.notes)

    for file in files_test:
        dbnns = md.file_to_dbnn(file)
        if dbnns == None:
            continue

        for dbnn in dbnns:
            dct[folder_test] += len(dbnn.notes)





print(dct)


exit()
for d in datasets:
    folder_train = "./../midi/" + d
    folder_test = "./../midi/" + d + "_val"

    files_train = [join(folder_train, f) for f in listdir(folder_train) if isfile(join(folder_train, f))]
    files_test = [join(folder_test, f) for f in listdir(folder_test) if isfile(join(folder_test, f))]

    X_n_train, X_l_train, X_d_train, X_v_train, X_b_train, Y_n_train, Y_l_train, Y_d_train, Y_v_train = data.load_data(files_train, sequence_length=30, samples_max=20, transpose = (0,1))
    X_n_test, X_l_test, X_d_test, X_v_test, X_b_test, Y_n_test, Y_l_test, Y_d_test, Y_v_test = data.load_data(files_test, sequence_length = 30, samples_max = 20, transpose = (0,1))

    dct[folder_train] = len(X_n_train)
    dct[folder_test] = len(X_n_test)


print(dct)


'''
{'./../midi/chopin': 117760, './../midi/chopin_val': 18560, './../midi/beethoven': 177920, './../midi/beethoven_val': 19840, './../midi/schubert': 149760, './../midi/schubert_val': 21120,
'./../midi/classic': 171520, './../midi/classic_val': 24960, './../midi/romantic': 230400, './../midi/romantic_val': 37760, './../midi/all': 837760, './../midi/all_val': 128640}
'''
