import log_reader
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

import numpy as np

log_values = log_reader.read_logs()

def mark_max(ax, log_values, name, metric, color):
    ax.plot(np.argmax(log_values[name][metric]),
            np.max(log_values[name][metric]), marker="o", color=color)

def mark_min(ax, log_values, name, metric, color):
    ax.plot(np.argmin(log_values[name][metric]),
            np.min(log_values[name][metric]), marker="o", color=color)

#############
#Noodipõhise ja kombineeritud mudeli
###########
fig = plt.figure(tight_layout=True)
gs = gridspec.GridSpec(3, 2)

ax = fig.add_subplot(gs[0, 0])
plt.title("Noodipikkuste täpsus")

ax.plot(log_values["classical_note_model_oige"]["val_output_lengths_categorical_accuracy"], label = "Klassitsism noodipõhine", color = "orange")
ax.plot(log_values["classic_combined_oige"]["val_output_lengths_categorical_accuracy"], label = "Klassitsism kombineeritud", linestyle = "--", color = "orange")

ax.plot(log_values["all_note_model"]["val_output_lengths_categorical_accuracy"], label = "Kõik noodipõhine", color = "red")
ax.plot(log_values["all_combined"]["val_output_lengths_categorical_accuracy"], label = "Kõik kombineeritud", linestyle = "--", color = "red")

ax.plot(log_values["chopin_note_model"]["val_output_lengths_categorical_accuracy"], label = "Chopin noodipõhine", color = "green")
ax.plot(log_values["chopin_combined"]["val_output_lengths_categorical_accuracy"], label = "Chopin kombineeritud", linestyle = "--", color = "green")

mark_max(ax, log_values, "classical_note_model_oige", "val_output_lengths_categorical_accuracy", "orange")
mark_max(ax, log_values, "classic_combined_oige", "val_output_lengths_categorical_accuracy", "orange")
mark_max(ax, log_values, "all_note_model", "val_output_lengths_categorical_accuracy", "red")
mark_max(ax, log_values, "all_combined", "val_output_lengths_categorical_accuracy", "red")
mark_max(ax, log_values, "chopin_note_model", "val_output_lengths_categorical_accuracy", "green")
mark_max(ax, log_values, "chopin_combined", "val_output_lengths_categorical_accuracy","green")


ax.set_xlim(-5, 100)
ax.set_xlabel("Epohh")
ax.set_ylabel("Täpsus")
ax.grid()

ax = fig.add_subplot(gs[1, 0])
plt.title("Viivituste täpsus")

ax.plot(log_values["classical_note_model_oige"]["val_output_delays_categorical_accuracy"], label = "Klassitsism noodipõhine", color = "orange")
ax.plot(log_values["classic_combined_oige"]["val_output_delays_categorical_accuracy"], label = "Klassitsism kombineeritud", linestyle = "--", color = "orange")

ax.plot(log_values["all_note_model"]["val_output_delays_categorical_accuracy"], label = "Kõik noodipõhine", color = "red")
ax.plot(log_values["all_combined"]["val_output_delays_categorical_accuracy"], label = "Kõik kombineeritud", linestyle = "--", color = "red")

ax.plot(log_values["chopin_note_model"]["val_output_delays_categorical_accuracy"], label = "Chopin noodipõhine", color = "green")
ax.plot(log_values["chopin_combined"]["val_output_delays_categorical_accuracy"], label = "Chopin kombineeritud", linestyle = "--", color = "green")

ax.set_xlim(-5, 100)
ax.set_xlabel("Epohh")
ax.set_ylabel("Täpsus")
ax.grid()
plt.legend(loc='upper center', bbox_to_anchor=(1, -0.2), ncol=3)

mark_max(ax, log_values, "classical_note_model_oige", "val_output_delays_categorical_accuracy", "orange")
mark_max(ax, log_values, "classic_combined_oige", "val_output_delays_categorical_accuracy", "orange")
mark_max(ax, log_values, "all_note_model", "val_output_delays_categorical_accuracy", "red")
mark_max(ax, log_values, "all_combined", "val_output_delays_categorical_accuracy", "red")
mark_max(ax, log_values, "chopin_note_model", "val_output_delays_categorical_accuracy", "green")
mark_max(ax, log_values, "chopin_combined", "val_output_delays_categorical_accuracy","green")

ax = fig.add_subplot(gs[0, 1])
plt.title("Kogu kahju")
ax.set_xlim(-5,100)
ax.grid()

ax.set_xlabel("Epohh")

ax.plot(log_values["classical_note_model_oige"]["val_loss"], label = "Klassitsism noodipõhine", color = "orange")
ax.plot(log_values["classic_combined_oige"]["val_loss"], label = "Klassitsism kombineeritud", linestyle = "--", color = "orange")

ax.plot(log_values["all_note_model"]["val_loss"], label = "Kõik noodipõhine", color = "red")
ax.plot(log_values["all_combined"]["val_loss"], label = "Kõik kombineeritud", linestyle = "--", color = "red")

ax.plot(log_values["chopin_note_model"]["val_loss"], label = "Chopin noodipõhine", color = "green")
ax.plot(log_values["chopin_combined"]["val_loss"], label = "Chopin kombineeritud", linestyle = "--", color = "green")

mark_min(ax, log_values, "classical_note_model_oige", "val_loss", "orange")
mark_min(ax, log_values, "classic_combined_oige", "val_loss", "orange")
mark_min(ax, log_values, "all_note_model", "val_loss", "red")
mark_min(ax, log_values, "all_combined", "val_loss", "red")
mark_min(ax, log_values, "chopin_note_model", "val_loss", "green")
mark_min(ax, log_values, "chopin_combined", "val_loss","green")
ax.set_xlim(-5, 100)


ax = fig.add_subplot(gs[1, 1])
plt.title("Noodikõrguse täpsus")
ax.set_xlim(-5,100)
ax.grid()

ax.set_xlabel("Epohh")



ax.plot(log_values["classical_note_model_oige"]["val_output_notes_categorical_accuracy"], label = "Klassitsism noodipõhine", color = "orange")
ax.plot(log_values["classic_combined_oige"]["val_output_notes_categorical_accuracy"], label = "Klassitsism kombineeritud", linestyle = "--", color = "orange")

ax.plot(log_values["all_note_model"]["val_output_notes_categorical_accuracy"], label = "Kõik noodipõhine", color = "red")
ax.plot(log_values["all_combined"]["val_output_notes_categorical_accuracy"], label = "Kõik kombineeritud", linestyle = "--", color = "red")

ax.plot(log_values["chopin_note_model"]["val_output_notes_categorical_accuracy"], label = "Chopin noodipõhine", color = "green")
ax.plot(log_values["chopin_combined"]["val_output_notes_categorical_accuracy"], label = "Chopin kombineeritud", linestyle = "--", color = "green")

mark_max(ax, log_values, "classical_note_model_oige", "val_output_notes_categorical_accuracy", "orange")
mark_max(ax, log_values, "classic_combined_oige", "val_output_notes_categorical_accuracy", "orange")
mark_max(ax, log_values, "all_note_model", "val_output_notes_categorical_accuracy", "red")
mark_max(ax, log_values, "all_combined", "val_output_notes_categorical_accuracy", "red")
mark_max(ax, log_values, "chopin_note_model", "val_output_notes_categorical_accuracy", "green")
mark_max(ax, log_values, "chopin_combined", "val_output_notes_categorical_accuracy","green")
ax.set_xlim(-5, 100)


plt.show()

