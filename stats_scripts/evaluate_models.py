import sys
sys.path.append("..")

from models.note_model import get_model
#from combined_model import get_prediction_model as get_model_comb
from models.combined_model import get_model as get_model_comb
import numpy as np

from src import combined_model_data as data_combined, note_model_data as data_note

from os import listdir
from os.path import isfile, join
from models import bar_model

num_CPU = 1
num_GPU = 0
num_cores = 4




#kombineeritudmudel koos bar_modeliga
datasets = ["chopin", "beethoven", "schubert", "classic", "romantic", "all"]
datasets = ["bach"]
all_res = {}
results = []
m_comb = get_model_comb(0.001, False)
bar_mdl = bar_model.get_model(0.01, False)
'''
for d in datasets:

    m_comb.load_weights('./../weights/' + d + '_combined.h5')
    folder_test = "./../midi/" + d + "_val/"
    files_test = [join(folder_test, f) for f in listdir(folder_test) if isfile(join(folder_test, f))]

    X_n_test, X_l_test, X_d_test, X_v_test, X_b_test, Y_n_test, Y_l_test, Y_d_test, Y_v_test = data_combined.load_data(
        files_test,
        sequence_length=30,
        samples_max=20,
        transpose=(
            0, 0))

    X_b_test = [x[:-1] for x in X_b_test]
    predicted_bars = bar_mdl.predict(X_b_test)

    for i, bars in enumerate(X_b_test):
        bars.append(predicted_bars[i])


    result = m_comb.evaluate({'input_notes': np.array(X_n_test),
                              'input_lenghts': np.array(X_l_test),
                              'input_delays': np.array(X_d_test),
                              'input_velo': np.array(X_v_test),
                              'input_bar': np.array(X_b_test),
                              },
                             [np.array(Y_n_test), np.array(Y_l_test), np.array(Y_d_test), np.array(Y_v_test)])

    results.append(result)

s = np.array(results)
np.save("bar_combined_results", s)


exit()
'''
#############mõlemad mudelid
m1 = get_model(0.001, False, batch_shape=(32, 30, 50))
m_comb = get_model_comb(0.001, False, batch_shape=(32, 30, 50))

datasets = ["chopin", "beethoven", "schubert", "classic", "romantic", "all"]
datasets = ["bach"]
results = []

for d in datasets:

    folder_test = "./../midi/" + d + "_val"
    files_test = [join(folder_test, f) for f in listdir(folder_test) if isfile(join(folder_test, f))]

    m1.load_weights('./../weights/' + d + '_note.h5')
    m_comb.load_weights('./../weights/' + d + '_combined.h5')

    X_n_test, X_l_test, X_d_test, X_v_test, Y_n_test, Y_l_test, Y_d_test, Y_v_test = data_note.load_data(files_test, sequence_length = 30,samples_max = 20, transpose = (0,0))
    result = m1.evaluate(    { 'input_notes' : np.array(X_n_test),
          'input_lenghts' : np.array(X_l_test),
          'input_delays' : np.array(X_d_test),
           'input_velo': np.array(X_v_test)
          },
        [np.array(Y_n_test), np.array(Y_l_test), np.array(Y_d_test), np.array(Y_v_test)])

    results.append([x for x in result])

    X_n_test, X_l_test, X_d_test, X_v_test, X_b_test, Y_n_test, Y_l_test, Y_d_test, Y_v_test = data_combined.load_data(files_test,
                                                                                                         sequence_length=30,
                                                                                                         samples_max=20,
                                                                                                         transpose=(
                                                                                                         0, 0))
    result = m_comb.evaluate({'input_notes': np.array(X_n_test),
                         'input_lenghts': np.array(X_l_test),
                         'input_delays': np.array(X_d_test),
                         'input_velo': np.array(X_v_test),
                         'input_bar' : np.array(X_b_test),
                         },
                        [np.array(Y_n_test), np.array(Y_l_test), np.array(Y_d_test), np.array(Y_v_test)])

    results.append([x for x in result])


s = np.array(results)
np.save("eval_results", s)
