import log_reader
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from colors import color

log_values = log_reader.read_logs()

#############
#Noodipõhise ja kombineeritud mudeli
###########
fig = plt.figure(tight_layout=True)
gs = gridspec.GridSpec(3, 3)

ax = fig.add_subplot(gs[0, 0])

color['orange'] = "C0"
color['green'] = "C1"
color['blue'] = "C2"



plt.title("Noodikõrguste täpsus")
ax.plot(log_values["all_note_model"]["val_output_notes_categorical_accuracy"], label = "Kõik", color = color['orange'])
ax.plot(log_values["all_note_1000"]["val_output_notes_categorical_accuracy"], label = "Kõik v2", linestyle = "--", color = color['orange'])

ax.plot(log_values["classical_note_model_oige"]["val_output_notes_categorical_accuracy"], label = "Klassitsism", color = color['green'])
ax.plot(log_values["classic_note_1001"]["val_output_notes_categorical_accuracy"], label = "Klassitsism v2", linestyle = "--", color = color['green'])

ax.plot(log_values["chopin_note_model"]["val_output_notes_categorical_accuracy"], label = "Chopin",  color = color['blue'])
ax.plot(log_values["chopin_note_1001"]["val_output_notes_categorical_accuracy"], label = "Chopin v2", linestyle = "--", color = color['blue'])


ax.set_xlim(-1, 30)
ax.set_xlabel("Epohh")
ax.set_ylabel("Noodipõhise mudeli täpsused")
ax.grid()


ax = fig.add_subplot(gs[0, 1])
plt.title("Viivituste täpsus")
ax.plot(log_values["all_note_model"]["val_output_delays_categorical_accuracy"], label = "Kõik", color = "C0")
ax.plot(log_values["all_note_1000"]["val_output_delays_categorical_accuracy"], label = "Kõik v2", linestyle = "--", color = "C0")

ax.plot(log_values["classical_note_model_oige"]["val_output_delays_categorical_accuracy"], label = "Klassitsism", color = "C1")
ax.plot(log_values["classic_note_1001"]["val_output_delays_categorical_accuracy"], label = "Klassitsism v2", linestyle = "--", color = "C1")

ax.plot(log_values["chopin_note_model"]["val_output_delays_categorical_accuracy"], label = "Chopin",  color = "C2")
ax.plot(log_values["chopin_note_1001"]["val_output_delays_categorical_accuracy"], label = "Chopin v2", linestyle = "--", color = "C2")


ax.set_xlim(-1, 30)
ax.set_xlabel("Epohh")
ax.grid()

ax = fig.add_subplot(gs[0, 2])
plt.title("Noodipikkuste täpsus")
ax.plot(log_values["all_note_model"]["val_output_lengths_categorical_accuracy"], label = "Kõik", color = "C0")
ax.plot(log_values["all_note_1000"]["val_output_lengths_categorical_accuracy"], label = "Kõik v2", linestyle = "--", color = "C0")

ax.plot(log_values["classical_note_model_oige"]["val_output_lengths_categorical_accuracy"], label = "Klassitsism", color = "C1")
ax.plot(log_values["classic_note_1001"]["val_output_lengths_categorical_accuracy"], label = "Klassitsism v2", linestyle = "--", color = "C1")

ax.plot(log_values["chopin_note_model"]["val_output_lengths_categorical_accuracy"], label = "Chopin",  color = color['blue'])
ax.plot(log_values["chopin_note_1001"]["val_output_lengths_categorical_accuracy"], label = "Chopin v2", linestyle = "--", color = color['blue'])

ax.set_xlim(-1, 30)
ax.set_xlabel("Epohh")
ax.grid()





ax = fig.add_subplot(gs[1, 0])

ax.set_xlim(-1, 30)
ax.grid()
ax.set_ylabel("Kombineeritud mudeli täpsused")
ax.set_xlabel("Epohh")

ax.plot(log_values["all_combined"]["val_output_notes_categorical_accuracy"], label = "Kõik v2", color = color['orange'])
ax.plot(log_values["all_combined_1000"]["val_output_notes_categorical_accuracy"], label = "Kõik v2", linestyle = "--", color = color['orange'])

ax.plot(log_values["classic_combined_oige"]["val_output_notes_categorical_accuracy"], label = "Klassitsism", color = color['green'])
ax.plot(log_values["classic_combined_1000"]["val_output_notes_categorical_accuracy"], label = "Klassitsism v2", color = color['green'], linestyle = "--")

ax.plot(log_values["chopin_combined"]["val_output_notes_categorical_accuracy"], label = "Chopin",  color = color['blue'])
ax.plot(log_values["chopin_combined_1000"]["val_output_notes_categorical_accuracy"], label = "Chopin v2", linestyle = "--", color = color['blue'])

ax = fig.add_subplot(gs[1, 1])

ax.set_xlim(-1,30)
ax.grid()
ax.set_xlabel("Epohh")
ax.plot(log_values["all_combined"]["val_output_delays_categorical_accuracy"], label = "Kõik A", color = 'C0')
ax.plot(log_values["all_combined_1000"]["val_output_delays_categorical_accuracy"], label = "Kõik B", linestyle = "--", color = 'C0')

ax.plot(log_values["classic_combined_oige"]["val_output_delays_categorical_accuracy"], label = "Klassitsism A", color = 'C1')
ax.plot(log_values["classic_combined_1000"]["val_output_delays_categorical_accuracy"], label = "Klassitsism B", color = 'C1', linestyle = "--")

ax.plot(log_values["chopin_combined"]["val_output_delays_categorical_accuracy"], label = "Chopin A",  color = 'C2')
ax.plot(log_values["chopin_combined_1000"]["val_output_delays_categorical_accuracy"], label = "Chopin B", linestyle = "--", color = 'C2')

ax = fig.add_subplot(gs[1, 2])

ax.set_xlim(-1,30)
ax.grid()

ax.set_xlabel("Epohh")
ax.plot(log_values["all_combined"]["val_output_lengths_categorical_accuracy"], label = "Kõik A", color = color['orange'])
ax.plot(log_values["all_combined_1000"]["val_output_lengths_categorical_accuracy"], label = "Kõik B", linestyle = "--", color = color['orange'])

ax.plot(log_values["classic_combined_oige"]["val_output_lengths_categorical_accuracy"], label = "Klassitsism A", color = color['green'])
ax.plot(log_values["classic_combined_1000"]["val_output_lengths_categorical_accuracy"], label = "Klassitsism B", color = color['green'], linestyle = "--")

ax.plot(log_values["chopin_combined"]["val_output_lengths_categorical_accuracy"], label = "Chopin A",  color = color['blue'])
ax.plot(log_values["chopin_combined_1000"]["val_output_lengths_categorical_accuracy"], label = "Chopin B", linestyle = "--", color = color['blue'])
plt.legend(loc='upper center', bbox_to_anchor=(-0.8, -0.3), ncol=3)

plt.show()

