from os import listdir
import tensorflow as tf
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

def read_logs():
    log_dir = "./../logs/"
    log_files = [
        "classical_note_model_oige",
        "classic_combined_oige",
        "chopin_note_model",
        "chopin_combined",
        "beethoven_combined",
        "beethoven_note_model",
        "romantic_combined",
        "romantic_note_model",
        "schubert_combined",
        "schubert_note_model",
        "all_combined",
        "all_note_model",
        "classic_note_1001",
        "classic_combined_1000",
        "chopin_note_1001",
        "chopin_combined_1000",
        "beethoven_combined_1000",
        "beethoven_note_1001",
        "romantic_combined_1000",
        "romantic_note_1000",
        "schubert_combined_1000",
        "schubert_note_1000",
        "all_combined_1000",
        "all_note_1000"
    ]

    logs = []

    tags = [
        "val_output_notes_categorical_accuracy",
        "val_output_delays_categorical_accuracy",
        "val_output_lengths_categorical_accuracy",
        "val_output_velo_mse",
        "val_loss",
        "output_notes_categorical_accuracy",
        "output_delays_categorical_accuracy",
        "output_lengths_categorical_accuracy",
        "val_output_velo_mean_absolute_error",
    ]

    log_values = dict()
    for log_name in log_files:
        log_values[log_name] = dict()
        for tag in tags:
            log_values[log_name][tag] = []

    for log_file in log_files:
        logs.append(log_dir + log_file + "/" + listdir(log_dir + log_file)[0])

    for name_index, log in enumerate(logs):
        for i, event in enumerate(tf.train.summary_iterator(log)):
            for value in event.summary.value:
                if value.tag in tags:
                    log_values[log_files[name_index]][value.tag].append(value.simple_value)

    return log_values



