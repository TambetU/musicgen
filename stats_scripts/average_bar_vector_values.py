import sys
sys.path.append("..")


import matplotlib.pyplot as plt
from os import listdir
from os.path import isfile, join
from src import midi_converter, bars
import numpy as np

folder = "./../midi/all"
files = [join(folder, f) for f in listdir(folder) if isfile(join(folder, f))]


k = np.array([0] * 12)
c = 0
for i, file in enumerate(files):
    print(i, len(files))
    dbnn = midi_converter.file_to_dbnn(file)[0]
    bar_notes = bars.divide_dbnn_into_bars(dbnn)
    bar_vecs = bars.bar_notes_to_bars(bar_notes, dbnn)
    bar_vecs = [sorted(x) for x in bar_vecs]

    for v in bar_vecs:
        k = np.add(k, v)
        c = c + 1

for fl in k / c:
    print(round(fl, 2))

plt.bar(range(12), k/c)
plt.show()








