import sys
sys.path.append("..")

from models import bar_prediction

from src import midi_converter, bars

dbnn = midi_converter.file_to_dbnn('./../midi/all/schuim-3.mid')[0]
bs = bar_prediction.generate(100, './../bar_model_weights/schubert.h5', dbnn)



bar_notes = bars.divide_dbnn_into_bars(dbnn)
bar_vecs = bars.bar_notes_to_bars(bar_notes, dbnn)

bar_vecs = bar_vecs[:40]

import matplotlib.pyplot as plt
plt.rcParams["figure.figsize"] = [6,6]
fig, ax = plt.subplots()


#värv #ff990d
plt.plot([12, 12], [0, 12], lw=1, ls = "--", color = "black")
for n in range(12):
    ax.broken_barh([(ind + 12, 0.9) for ind, x in enumerate(bs)], (n-0.25, 0.25),
                   facecolors=[(1,0.6,0.05, min(1, x[n])) for ind, x in enumerate(bs)])

for n in range(12):
    ax.broken_barh([(ind, 0.9) for ind, x in enumerate(bar_vecs)], (n, 0.25),
                   facecolors=[(0.1,0.6,0.5, min(1, x[n])) for ind, x in enumerate(bar_vecs)])

ax.set_ylabel('Noodi nimi')
ax.set_xlabel('Takti number')
ax.set_yticks([0,1,2,3,4,5,6,7,8,9,10,11])
ax.set_yticklabels(['C', 'C#','D', 'D#','E', 'F','F#', 'G','G#', 'A','A#', 'H'])

plt.show()
