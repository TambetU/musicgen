


color = {
    'orange' : (211,84,0),
    'red' : (231,76,60),
    'green' : (39,174,96),
    'yellow' : (241,196,15),
    'blue' : (41,128,185),
    'purple' : (142,68,173),
    'dark' : (44,62,80)
}

color = { key:[v / 255 for v in value] for (key, value) in color.items()}
