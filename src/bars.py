from src.note_durations import duration_lengths

def calc_bar_duration(dbnn):
    return dbnn.timing.numerator / dbnn.timing.denominator

def bars_from_dbnn(dbnn):
    bar_duration = calc_bar_duration(dbnn)
    # tagasta taktide kaupa noodid
    bars = []

    bar_cur_duration = 0
    bar_notes = [0] * 15
    highest = 0

    notes = []
    notes_lists = []

    for note in dbnn.notes:

        note_delay = duration_lengths[note.delay]

        # kui delay summa suurem kui takti pikkus, siis läheb järgmisse:
        if bar_cur_duration + note_delay - bar_duration > 0:
            notes.sort(key = lambda x : x.note, reverse = True)
            bar_notes[14] = sum([x.note for x in notes]) / len(notes) / 100
            bar_notes[13] = len(notes)
            bar_notes[12] = notes[0].note / 100
            bars.append(bar_notes)
            bar_notes = [0] * 15
            notes = []
            bar_notes[note.note % 12] += note_delay + bar_cur_duration + duration_lengths[note.duration] / dbnn.timing.denominator - bar_duration
            bar_cur_duration = note_delay + bar_cur_duration - bar_duration


        else:
            bar_cur_duration += note_delay
            bar_notes[note.note % 12] += duration_lengths[note.duration] / dbnn.timing.denominator

        notes.append(note)


    return bars

def is_note_from_next_bar(note_delay, note, bar_cur_duration, bar_duration):
    diff = bar_cur_duration + note_delay - bar_duration > -1 * duration_lengths[1];

    is_small = abs(bar_cur_duration + note_delay - bar_duration) * 5 < duration_lengths[note.duration]

    if diff or is_small:
        return True
    else:
        return False

def get_last_bar_vector():
    return [0] * 12

# Jagab teose noodid taktide kaupa
def divide_dbnn_into_bars(dbnn):

    bar_duration = calc_bar_duration(dbnn)
    bar_cur_duration = 0
    bars = []
    notes = []

    for i, note in enumerate(dbnn.notes):
        note_delay = duration_lengths[note.delay]

        if is_note_from_next_bar(note_delay, note, bar_cur_duration, bar_duration):
            bars.append(notes)
            notes = []
            bar_cur_duration = note_delay + bar_cur_duration - bar_duration
        else:
            bar_cur_duration += note_delay

        notes.append((bar_cur_duration, duration_lengths[note.duration] / bar_duration, note.note))

        if i == len(dbnn.notes) - 1:
            bars.append(notes)

    return bars

# Teeb iga takti kohta vektori
def bar_notes_to_bars(bars, dbnn):
    bar_duration = calc_bar_duration(dbnn)
    sum_bars = []
    for bar in bars:
        sum_bar = [0] * 12

        for note in bar:
            bar_part = 0
            if note[0]+note[1] > bar_duration:
                bar_part = bar_duration - note[0]
            else:
                bar_part = note[1]
            sum_bar[note[2] % 12] = min(sum_bar[note[2]%12] + bar_part, 1)

        '''
        s = sorted(sum_bar, reverse = True)
        sum_bar = [1 if x != 0 and (x > s[3] or x > 0.4) else 0 for x in sum_bar]
        '''

        sum_bars.append(sum_bar)

    return sum_bars

'''
midi = mido.MidiFile("./midi/mond_1.mid")

d = midi_converter.dbnn_from_midi_object(midi)[0]

note_bars = divide_dbnn_into_bars(d)
bs = bar_notes_to_bars(note_bars, d)


import matplotlib.pyplot as plt
#plt.style.use('dark_background')

fig, ax = plt.subplots()

plt.plot([x[12] * 12 for x in bs])
plt.plot([x[13] / 12 for x in bs])
plt.plot([x[14] * 12 for x in bs])

#värv #ff990d   

for n in range(12):
    ax.broken_barh([(ind, 0.9) for ind, x in enumerate(bs)], (n-0.25, 0.5),
                   facecolors=[(1,0.6,0.05, min(1, x[n] * 1)) for ind, x in enumerate(bs)])

for i, bar in enumerate(note_bars):
    for note in bar:
        ax.broken_barh([(i + note[0], note[1])], (note[2]%12-0.1, 0.2), facecolors = 'red')


ax.set_title("Teos taktides vektoresitusena")
ax.set_xlim(0, 10)
ax.set_ylabel('Noodi nimi')
ax.set_xlabel('Takti number')
ax.set_yticks([0,1,2,3,4,5,6,7,8,9,10,11])
ax.set_yticklabels(['C', 'C#','D', 'D#','E', 'F','F#', 'G','G#', 'A','A#', 'H'])
#ax.grid(True)
ax.annotate('race interrupted', (61, 25),
            xytext=(0.8, 0.9), textcoords='axes fraction',
            arrowprops=dict(facecolor='black', shrink=0.001),
            fontsize=16,
            horizontalalignment='right', verticalalignment='top')
plt.show()
from midi_output import MidiOutput
mo = MidiOutput(0)
mid = mido.MidiFile()
track = mido.MidiTrack()
mid.tracks.append(track)

print(len(bs))

for bar in bs:
    first = True
    for i, b in enumerate(bar):
        if(b > 0.02):
            if first:
                track.append(mido.Message(type = "note_on", note = 60 + i, velocity = int(80 * b), time = 0))
            else:
                track.append(mido.Message(type = "note_on", note = 60 + i, velocity = int(80 * b), time = 0))

            first = False
    
    first = True
    for i, b in enumerate(bar):
        if(b > 0.02):
            if first:
                track.append(mido.Message(type = "note_off", note = 60 + i, velocity = 80, time = 1000))
            else:
                track.append(mido.Message(type = "note_off", note = 60 + i, velocity = 80, time =0 ))

            first = False

for msg in mid.play():
    print(msg)
    mo.send_to_output(msg)


'''

