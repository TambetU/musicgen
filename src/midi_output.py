import mido


class MidiOutput:
    #types
    # 0 = rtmidi
    # 1 = first out

    def __init__(self, type = 0):
        if type == 0:
            rtmidi = mido.Backend('mido.backends.rtmidi')
            self.output = rtmidi.open_output()
        elif type == 1:
            self.output = mido.open_output(mido.get_output_names()[1])

    def save_to_file(self, midi, filename):
        midi.save(filename)

    def send_to_output(self, msg):
        self.output.send(msg)

    def play_midi(self, midi):
        for msg in midi.play():
            self.output.send(msg)







