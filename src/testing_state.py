from models.note_model import get_model
import numpy as np
import keras
from keras.callbacks import LearningRateScheduler

from src import note_model_data as data

from os import listdir
from os.path import isfile, join


def learning_rate(epoch):
    lrate = 0.008 * pow(0.95, epoch)
    print("lr :" + str(lrate))
    print("")
    return lrate

class StateReseting(keras.callbacks.Callback):
    def on_batch_begin(self, batch, logs=None):
        if batch % 10 == 0:
            self.model.reset_states()

mc = keras.callbacks.ModelCheckpoint('./weights/lamp{epoch:05d}.h5', save_weights_only=True, period=10)

m = get_model(0.001, False, batch_shape = (32, 2, 113))

#m.load_weights('./a_model_weights/v_m_weights00005.h5')


folder_train = "./midi/v"
folder_test = "./midi/chopin_val"
tensorCb = keras.callbacks.TensorBoard(log_dir='./logs/chopin_weights00012', histogram_freq=0, batch_size=32, write_graph=True, write_grads=False, write_images=False, embeddings_freq=0, embeddings_layer_names=None, embeddings_metadata=None)

files_train = [join(folder_train, f) for f in listdir(folder_train) if isfile(join(folder_train, f))]
files_test = [join(folder_test, f) for f in listdir(folder_test) if isfile(join(folder_test, f))]

X_n_train, X_l_train, X_d_train, X_v_train, Y_n_train, Y_l_train, Y_d_train, Y_v_train = data.load_data(files_train, sequence_length = 2, samples_max = 20, transpose = (0,1))
X_n_test, X_l_test, X_d_test, X_v_test, Y_n_test, Y_l_test, Y_d_test, Y_v_test = data.load_data(files_test, transpose = (0,1))

lrate = LearningRateScheduler(learning_rate)
callbacks = [mc, StateReseting()]


history = m.fit(
    {'input_notes': np.array(X_n_train),
     'input_lenghts': np.array(X_l_train),
     'input_delays': np.array(X_d_train),
     'input_velo': np.array(X_v_train)
     },
    [np.array(Y_n_train), np.array(Y_l_train), np.array(Y_d_train), np.array(Y_v_train)],
    verbose = 2,
    callbacks = callbacks,
    batch_size = 32,
    epochs = 1000,
    shuffle = False
)


