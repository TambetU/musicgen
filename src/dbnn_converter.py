import numpy as np
from src import note_durations as durations
from src.note_event import NoteEvent
from src.dbnn import Dbnn

notes_count = 88
duration_count = 12
# [
#  Noot : [88]
#  Kestus : [12]
#  Viivitus : [12]
#  Velocity :  0-1
# ]

def dbnn_to_vecs_dict(dbnn):
    d = {
        'n' : [],
        'l' : [],
        'd' : [],
        'v' : [],
    }

    for note in dbnn.notes:
        d['n'].append(notenr_to_vec(note.note))
        d['l'].append(duration_to_vec(note.duration))
        d['d'].append(duration_to_vec(note.delay))
        d['v'].append([note.velocity / 127])

    return d

def dbnn_to_flat_vecs_list(dbnn):
    v_list = []

    for note in dbnn.notes:
        v_list.append(note_to_flat_vec(note))

    return v_list

def dbnn_bar_combination_to_flat_vecs_list(comb_tuples):
    v_list = []
    for tuple in comb_tuples:
        el = note_to_flat_vec(tuple[0])
        el.extend(tuple[1])
        v_list.append(el)

    return v_list

def flat_vecs_list_to_dbnn(vlist):
    #TODO taktimõõt, temppo, ticks_per_beat
    dbnn = Dbnn()
    dbnn.timing = durations.NoteDurations(4,4,100000,8,480)

    for vecs in vlist:
        dbnn.notes.append(vecs_to_note(vecs))


    return dbnn

def vecs_dict_to_dbnn(dict, temp = 0.9):
    dbnn = Dbnn()
    dbnn.timing = durations.NoteDurations(4,4,100000, 8, 480)

    for i, s in enumerate(dict['n']):
        note = NoteEvent()
        note.note = notevec_to_notenr(dict['n'][i], temp)
        note.duration = vec_to_duration(dict['l'][i], temp)
        note.delay = vec_to_duration(dict['d'][i], temp)
        note.velocity = int(127 * (dict['v'][i][0]))
        dbnn.notes.append(note)

    return dbnn



def note_to_flat_vec(note):
    s = []
    s.extend(notenr_to_vec(note.note))
    s.extend(duration_to_vec(note.duration))
    s.extend(duration_to_vec(note.delay))
    s.append(note.velocity / 127)
    return s

def vecs_to_note(vecs):
    note = NoteEvent()
    note.note = notevec_to_notenr(vecs[0:notes_count])
    note.duration = vec_to_duration(vecs[notes_count:notes_count+duration_count])
    note.delay = vec_to_duration(vecs[notes_count+duration_count:notes_count+duration_count*2])
    note.velocity = int(127 * vecs[notes_count+duration_count*2])
    return note

def notenr_to_vec(notenr):
    vector = [0] * notes_count
    vector[notenr - 21] = 1
    return vector

def notevec_to_notenr(vec, temp = 0.9):
    return multinomialrandom(vec, temp) + 21

def duration_to_vec(note_duration):
    vector = [0] * duration_count
    vector[note_duration] = 1
    return vector

def vec_to_duration(vec, temp = 0.9):
    return durations.Duration(multinomialrandom(vec, temp))

def multinomialrandom(vec, temp):
    if temp == 1:
        return np.argmax(vec)

    v = np.array(vec, dtype = np.float64)
    exp = np.exp(np.log(v) / temp)
    tn = exp / np.sum(exp)
    return np.argmax(np.random.multinomial(1, tn, 1))



    




