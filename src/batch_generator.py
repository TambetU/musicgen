# import keras.utils.Sequence as Sequence


class VariableWindowBatchGeneretor:
    def __init__(self, notes_count, lengths_count, window_size=100):
        self.max_window_size = 150
        self.notes_count = notes_count
        self.durations_count = lengths_count

        # iga erineva järjendi pikkuse kohta järjend

        self.X = [[]] * self.max_window_size
        self.Y_n = [[]] * self.max_window_size
        self.Y_d = [[]] * self.max_window_size
        self.Y_l = [[]] * self.max_window_size
        self.Y_v = [[]] * self.max_window_size

    # Noodivektorite list närvivõrgu sisend ja target listideks
    def vlist_to_x_y_list(self, v_list):

        window_size = 1
        max_window = self.max_window_size

        if len(v_list) < 2 * max_window:
            max_window = int(len(v_list) / 2)

        for i in range(0, len(v_list) - max_window):

            print(len(self.xy_sequences))
            self.xy_sequences[window_size - 1][0].append(v_list[i:i + window_size])
            self.xy_sequences[window_size - 1][1].append(v_list[i + window_size][0:self.notes_count])
            self.xy_sequences[window_size - 1][2].append(
                v_list[i + window_size][self.notes_count:self.notes_count + self.durations_count])
            self.xy_sequences[window_size - 1][3].append(v_list[i + window_size][
                                                         self.notes_count + self.durations_count:self.notes_count + self.durations_count * 2])
            self.xy_sequences[window_size - 1][4].append(v_list[i + window_size][-1])

            print(len(self.xy_sequences[1][0]))

            if window_size < max_window:
                self.xy_sequences.append([[[], [], [], [], []]])
                window_size += 1


'''
class SequenceGenerator(Sequence):

    def __init__(self, x_set, y_set, batch_size):
        self.x, self.y = x_set, y_set
        self.batch_size = batch_size
        self.r = 0

    def __len__(self):
        return int(np.ceil(len(self.x) / float(self.batch_size)))

    def __getitem__(self, idx):
        batch_x = self.x[idx * self.batch_size:(idx + 1) * self.batch_size]
        batch_y = self.y[idx * self.batch_size:(idx + 1) * self.batch_size]

        return np.array([
            resize(imread(file_name), (200, 200))
               for file_name in batch_x]), np.array(batch_y)
'''
