from src.note_event import NoteEvent
from src.dbnn import Dbnn
from src import note_durations
import random


def gen(length = 50):
    dbnn = Dbnn()
    dbnn.timing = note_durations.NoteDurations(4, 4, 400000, 8, 480)

    for i in range(0,length):
        note = NoteEvent()
        note.note = random.randint(40,90)
        note.delay = note_durations.Duration(random.randint(5, 7))
        note.duration = note_durations.Duration(random.randint(5, 7))
        note.velocity = random.randint(60,90)
        dbnn.notes.append(note)

    return dbnn


