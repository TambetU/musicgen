from enum import IntEnum


class Duration(IntEnum):
    ZERO = 0
    SIXTYFORTH = 1 
    THIRTYSECOND = 2 
    SIXTEENTH = 3 
    TRIPLET = 4 
    EIGHT = 5 
    EIGHT_DOT = 6 
    QUARTER = 7 
    QUARTER_DOT = 8 
    HALF = 9 
    HALF_DOT = 10 
    FULL = 11

duration_lengths = [ 
    0,
    1/64,
    2/64,
    4/64,
    16/192,
    8/64,
    12/64,
    16/64,
    24/64,
    32/64,
    48/64,
    1
]

'''
  'sixtyforth' :  [1,0,0,0,0,0,0,0,0,0,0],
  'thirtysecond': [0,1,0,0,0,0,0,0,0,0,0],
  'sixteenth' :   [0,0,1,0,0,0,0,0,0,0,0],
  'triple':       [0,0,0,1,0,0,0,0,0,0,0],
  'eight':        [0,0,0,0,1,0,0,0,0,0,0],
  'eight_dot':    [0,0,0,0,0,1,0,0,0,0,0],
  'quarte':       [0,0,0,0,0,0,1,0,0,0,0],
  'quarter_dot':  [0,0,0,0,0,0,0,1,0,0,0],
  'hal':          [0,0,0,0,0,0,0,0,1,0,0],
  'half_dot':     [0,0,0,0,0,0,0,0,0,1,0],
  'full' :        [0,0,0,0,0,0,0,0,0,0,1]
'''

class NoteDurations:
    def __init__(self, time_signature_numerator, time_signature_denominator, tempo, t32_notes_per_beat, ticks_per_beat):
        # Taktimõõt
        # Löökide arv taktis
        self.numerator = time_signature_numerator

        # Löögile vastav noot
        self.denominator = time_signature_denominator

        # 32ndik nootide arv ühes löögis
        self.t32_notes_per_beat = t32_notes_per_beat

        # Tempo (mikrosekundit löögi kohta)
        self.tempo = 500000

        # Löögi pikkus 64ndik nootides 
        self.beat = int(self.t32_notes_per_beat*2 / (self.denominator / 4))

        # Takti pikkus 64ndik nootides
        self.measure = self.numerator * self.beat 

        # Midi sammude arv löögis
        self.ticks_per_beat = ticks_per_beat

        # Midi sammude arv 64ndik noodis
        self.ticks_per_64th_note = self.ticks_per_beat / self.beat


    
    def calc_notated_duration(self, duration):
        if(duration < 0):
            duration = 0
            #raise ValueError("duration must be >=0.")
        
        #64/64
        if duration >= 62*self.ticks_per_64th_note:
            return Duration.FULL 
        #48/64
        if duration >= 45*self.ticks_per_64th_note:
            return Duration.HALF_DOT 
        #32/64
        if duration >= 29*self.ticks_per_64th_note:
            return Duration.HALF 
        #24/64
        if duration >= 22*self.ticks_per_64th_note:
            return Duration.QUARTER_DOT 
        #16/64
        if duration >= 14*self.ticks_per_64th_note:
            return Duration.QUARTER 
        #12/64
        if duration >= 10*self.ticks_per_64th_note:
            return Duration.EIGHT_DOT 
        #8/64
        if duration >= 8*self.ticks_per_64th_note:
            return Duration.EIGHT 
        #16/64 / 3
        if duration >= 5*self.ticks_per_64th_note:
            return Duration.TRIPLET 
        #4/64
        if duration >= 3*self.ticks_per_64th_note:
            return Duration.SIXTEENTH 
        #2/64 = 4/128
        if duration > 3*(self.ticks_per_64th_note/2):
            return Duration.THIRTYSECOND 
        #1/64
        if duration >= 2*self.ticks_per_64th_note/3:
            return Duration.SIXTYFORTH
        else:
            return Duration.ZERO 
    
    def notated_duration_to_ticks(self, notated_duration):
        return duration_lengths[notated_duration] * self.measure * self.ticks_per_64th_note  
        

#print(NoteDurations(4,4,8, 8, 10000).notated_duration_to_ticks(Duration.QUARTER.value))



