import mido

from src.dbnn import Dbnn
from src.note_event import NoteEvent
from src.note_durations import NoteDurations


def file_to_dbnn(file):
    try:
        midi = mido.MidiFile(file)
    except IOError:
        return None

    except EOFError:
        return None

    return dbnn_from_midi_object(midi)

def find_time_signature_changes(midi):

    time_signature_changes = []
    time = 0

    for msg in midi.tracks[0]:
        time += msg.time

        if(msg.type == "time_signature"):
            time_signature_changes.append((time, msg))
    return time_signature_changes

    
def dbnn_from_midi_object(midi):
    if midi.type == 0:
        end_track = 1

    unfinished_notes = []
    time_signature_changes = find_time_signature_changes(midi)
    dbnn_note_events = []

    for i, track in enumerate(midi.tracks):
        if i > 4:
            continue

        time = 0
        for message in track:
            time += message.time
            process_message(message, unfinished_notes, dbnn_note_events, time)

    dbnn_note_events.sort(key = lambda x : x.abs_start_time)

    last_note_time = dbnn_note_events[0].abs_start_time
    dbnn_parts = []

    i = 0
    for change_count, (sig_change_time, sig) in enumerate(time_signature_changes):
        song = Dbnn()
        song.timing = NoteDurations(sig.numerator, sig.denominator, 900000, sig.notated_32nd_notes_per_beat, midi.ticks_per_beat)
        #Noodipikkused
        while(True):
            if len(time_signature_changes) > (change_count + 1) and time_signature_changes[change_count+1][0] > last_note_time:
                if len(song.notes) > 30:
                    dbnn_parts.append(song)
                    break
            if i >= len(dbnn_note_events):
                break

            note = dbnn_note_events[i]
            dbnn_note_events[i].delay = note.abs_start_time - last_note_time
            last_note_time = dbnn_note_events[i].abs_start_time
            dbnn_note_events[i].duration = song.timing.calc_notated_duration(note.abs_end_time - note.abs_start_time)
            dbnn_note_events[i].delay = song.timing.calc_notated_duration(note.delay)
            song.notes.append(dbnn_note_events[i])
            i+=1

            if i == len(dbnn_note_events):
                if len(song.notes) > 30:
                    dbnn_parts.append(song)
                break

    return dbnn_parts
    
        
def process_message(message, unfinished_notes, notes, time):
    if message.type == "note_on" and message.velocity > 0:
        note = NoteEvent()
        note.delay = message.time
        note.from_midi_message(message)
        note.abs_start_time=time
        unfinished_notes.append(note)

    elif message.type == "note_on" or message.type == "note_off":
        note = next((x for x in unfinished_notes if x.note == message.note), False)
        if not note:
            print("Alustamata noot")
        else:
            note.abs_end_time = time
            unfinished_notes.remove(note)
            notes.append(note)
        return

    else:
        pass


def dbnn_to_midi_object(song):
    mid = mido.MidiFile()

    meta_track = mido.MidiTrack()
    mid.tracks.append(meta_track)

    meta_track.append(mido.MetaMessage(type="time_signature", time=0, denominator=song.timing.denominator,
                                       numerator=song.timing.numerator,
                                       notated_32nd_notes_per_beat=song.timing.t32_notes_per_beat))
    mid.ticks_per_beat = 480
    meta_track.append(mido.MetaMessage(type="set_tempo", tempo=song.timing.tempo))

    track = mido.MidiTrack()
    mid.tracks.append(track)
    track.append(mido.Message('program_change', program=0, time=0))

    notes = []
    time = 0
    for i, event in enumerate(song.notes):
        time += song.timing.notated_duration_to_ticks(event.delay)
        event.abs_start_time = time
        end_event = NoteEvent()
        end_event.type = 0
        end_event.abs_start_time = song.timing.notated_duration_to_ticks(event.duration) + time
        end_event.note = event.note
        notes.append(event)
        notes.append(end_event)

    notes.sort(key = lambda x : x.abs_start_time)

    times = [0]

    for i, note in enumerate(notes):
        if i == 0:
            continue

        times.append(note.abs_start_time - notes[i-1].abs_start_time)

    for i, note in enumerate(notes):
        if(note.type == 1):
            msg = mido.Message(type="note_on", note=note.note, velocity=note.velocity)

        if(note.type == 0):
            msg = mido.Message(type="note_off", note=note.note, velocity=0)

        msg.time = int(times[i])



        track.append(msg)

    return mid

def dbnn_to_midi_object_relative(song):

    mid = mido.MidiFile()

    meta_track = mido.MidiTrack()
    mid.tracks.append(meta_track)

    meta_track.append(mido.MetaMessage(type = "time_signature",time = 0, denominator = song.timing.denominator, numerator = song.timing.numerator, notated_32nd_notes_per_beat = song.timing.t32_notes_per_beat))
    meta_track.append(mido.MetaMessage(type="set_tempo", tempo = song.timing.tempo))


    track = mido.MidiTrack()
    mid.tracks.append(track)
    track.append(mido.Message('program_change', program=0, time=0))

    unfinished_notes = {}

    time = 0
    for i, event in enumerate(song.notes):
        time += song.timing.notated_duration_to_ticks(event.delay)

        unfinished_notes = {note:time for note,time in unfinished_notes.items() if time != -1}
        unfinished_notes[event.note] = time + song.timing.notated_duration_to_ticks(event.duration)

        msg = event_to_midi_message(song, i)
        msg.time = int(song.timing.notated_duration_to_ticks(event.delay))

        track.append(msg)

        for note, n_time in unfinished_notes.items():
            if n_time <= time:
                unfinished_notes[note] = -1
                track.append(mido.Message(type = "note_off", note = note, velocity = 0, time = 0))


    return mid

def event_to_midi_message(song, index):
    return mido.Message(type = "note_on", note = song.notes[index].note, velocity = song.notes[index].velocity)




'''
ddbn = file_to_dbnn('./midi/all/mond_3.mid')

midi = dbnn_to_midi_object(ddbn)

out = MidiOutput(0)
out.play_midi(midi)

'''







        