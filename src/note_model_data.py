from src import midi_converter, dbnn_transposer, dbnn_converter

from os import listdir
from os.path import isfile, join



'''
Andmeid vaja kujul : 
(num_samples, window_size, note_vec_len)

num_samples % batch_size = 0
sama teose järjestikused tükid peavad asetsema indeksitel (i_1 = x, i_2 = x + batch_size)
'''

# samples_max - suurim suurus sample'te arv teose kohta, kui suurem jagatakse mitmeks tükiks
#
#
def load_data(files, sequence_length = 20, batch_size = 32, samples_max = 50, transpose = (5,5)):

    X_n = []
    X_d = []
    X_l = []
    X_v = []
    Y_n = []
    Y_d = []
    Y_l = []
    Y_v = []

    notes_count = 88
    lengths_count = 12
    durations_count = 12
    dbnn_vector_sequences = []

    for i, file in enumerate(files, 1):
        print(file, str(i) + "/" + str(len(files)))

        if file.split('.')[-1] != 'mid':
            continue

        dbnns = midi_converter.file_to_dbnn(file)
        if dbnns == None:
            continue
        for dbnn_original in dbnns:
            transposed = dbnn_transposer.transpose_in_range(dbnn_original, transpose[0], transpose[1])

            for dbnn in transposed:

                v_dict_list = dbnn_converter.dbnn_to_vecs_dict(dbnn)

                for start_position in range(0, sequence_length):
                    combined_tuples_list = []

                    x_y_dict = {
                        'X_n': [],
                        'X_l': [],
                        'X_d': [],
                        'X_v': [],
                        'Y_n': [],
                        'Y_l': [],
                        'Y_d': [],
                        'Y_v': []
                    }

                    for ind in range(sequence_length + start_position, len(v_dict_list['n']), sequence_length):

                        x_y_dict['X_n'].append(v_dict_list['n'][ind - sequence_length : ind])
                        x_y_dict['X_l'].append(v_dict_list['l'][ind - sequence_length : ind])
                        x_y_dict['X_d'].append(v_dict_list['d'][ind - sequence_length : ind])
                        x_y_dict['X_v'].append(v_dict_list['v'][ind - sequence_length : ind])

                        x_y_dict['Y_n'].append(v_dict_list['n'][ind])
                        x_y_dict['Y_l'].append(v_dict_list['l'][ind])
                        x_y_dict['Y_d'].append(v_dict_list['d'][ind])
                        x_y_dict['Y_v'].append(v_dict_list['v'][ind])

                        if len(x_y_dict['X_n']) == samples_max:
                           dbnn_vector_sequences.append(x_y_dict)

                           x_y_dict = {
                               'X_n': [],
                               'X_l': [],
                               'X_d': [],
                               'X_v': [],
                               'Y_l': [],
                               'Y_d': [],
                               'Y_n': [],
                               'Y_v': []
                           }


    #loodud väärtuste jaotamine batch_size suuruste vahedega sisend jadasse


    for index in range(0, len(dbnn_vector_sequences) - batch_size, batch_size):
        for samples_index in range(0, samples_max):
            for dbnn_dict_index, dbnn_dict in enumerate(dbnn_vector_sequences[index:index + batch_size]):
                X_n.append(dbnn_dict['X_n'][samples_index])
                X_l.append(dbnn_dict['X_l'][samples_index])
                X_d.append(dbnn_dict['X_d'][samples_index])
                X_v.append(dbnn_dict['X_v'][samples_index])
                Y_n.append(dbnn_dict['Y_n'][samples_index])
                Y_d.append(dbnn_dict['Y_d'][samples_index])
                Y_l.append(dbnn_dict['Y_l'][samples_index])
                Y_v.append(dbnn_dict['Y_v'][samples_index])

    return (X_n, X_l, X_d, X_v, Y_n, Y_l, Y_d, Y_v)

def test_data_persistance():

    from src.midi_output import MidiOutput
    import mido

    folder_train = "./midi/all/"
    files_train = [join(folder_train, f) for f in listdir(folder_train) if isfile(join(folder_train, f))][12:20]
    data = load_data(files_train)
    rtmidi = mido.Backend('mido.backends.rtmidi')
    m = MidiOutput(0)

    for i in range(0, 50):
        index = i * 32 + 32

        dict = {
            'n' : [],
            'l' : [],
            'd' : [],
            'v' : []
        }


        dict['n'].extend(data[0][index])
        dict['l'].extend(data[1][index])
        dict['d'].extend(data[2][index])
        dict['v'].extend(data[3][index])
        ''' 
        dict['n'].append(data[4][index])
        dict['l'].append(data[5][index])
        dict['d'].append(data[6][index])
        dict['v'].append(data[7][index])
        '''

        dbnn = dbnn_converter.vecs_dict_to_dbnn(dict)
        midi = midi_converter.dbnn_to_midi_object(dbnn)

        i = 0
        for msg in midi.play():
            if msg.type == "note_on":
                print(dbnn.notes[i])
                i += 1

                m.send_to_output(msg)

#test_data_persistance()
