from src.dbnn import Dbnn
from src.note_event import NoteEvent

# esimene noot klaveril 21

def get_highest_note(dbnn):
    return max(dbnn.notes, key = lambda x : x.note).note-21

def get_lowest_note(dbnn):
    return min(dbnn.notes, key = lambda x : x.note).note-21
    
def get_tranposing_range(dbnn):
    return (get_lowest_note(dbnn) , 87-get_highest_note(dbnn))

def transpose(dbnn, ammount):
    for note in dbnn.notes:
        note.note += ammount

def transpose_in_range(dbnn, down, up):
    possible_range = get_tranposing_range(dbnn)

    if down > possible_range[0]:
        down = possible_range[0]
    if up > possible_range[1]:
        up = possible_range[1]

    down = -1 * down

    tranposed = [dbnn]

    for i in range(down, up + 1):
        if i == 0:
            continue
        n_dbnn = Dbnn()
        n_dbnn.timing = dbnn.timing

        for note in dbnn.notes:
            n_note = NoteEvent()
            n_note.note = note.note + i
            n_note.duration = note.duration
            n_note.delay = note.delay
            n_note.velocity = note.velocity
            n_dbnn.notes.append(n_note)

        tranposed.append(n_dbnn)

    return tranposed
