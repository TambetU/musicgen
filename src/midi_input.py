import mido
import time
from src.midi_utils import midi_note_nr_to_name

class MidiInput:
    def __init__(self):
        self.io = 0

    def open_output(self):
        print(mido.get_output_names())
        output = mido.open_output(mido.get_output_names()[1])
        return output

    def listen(self, tempo, amount = 100):
        port = mido.open_input()   
        mid = mido.MidiFile()
        track = mido.MidiTrack()
        mid.tracks.append(track)
        track.append(mido.Message('program_change', program=0, time=0))
        track.append(mido.MetaMessage('time_signature', numerator = 4, denominator = 4, notated_32nd_notes_per_beat=8))
        mid.ticks_per_beat = 240
        timer = time.time()

        count = 0


        for msg in port:
            if(msg.type == "control_change" and msg.value == 0 or count > amount):
                port.close()
                return mid


            if(msg.type == "note_on" or msg.type == "note_off"):
                if(msg.type == "note_on" and msg.velocity != 0):
                    count += 1
                    print(midi_note_nr_to_name(msg.note))

                msg.time = int(mido.second2tick(time.time() - timer, mid.ticks_per_beat, tempo))
                track.append(msg)
                timer = time.time()

