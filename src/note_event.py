from src import midi_utils


class NoteEvent:
    def __init__(self):

        # Noot = 1 või paus = 0
        self.type = 1

        # Kestus, üks määratud pikkustest NoteDuration.py
        self.duration = 0

        # Noot, [0-87] vastavalt klaveriklahvidele
        self.note = 0

        # Velocity (valjus) [0-127]
        self.velocity = 80 

        # Aeg eelneva noodi algusest
        self.delay = 0

        # samm, millal algas
        self.abs_start_time = 0

        # samm, millal lõppes
        self.abs_end_time = 0

    # Teeb NoteEvent-i mido midi eventist
    def from_midi_message(self, midi_message):
        self.type = 1
        self.note = midi_message.note
        self.velocity = midi_message.velocity

    def __eq__(self, other):
        return self.note == other.note
    
    def __str__(self):
        return '{:5}'.format(midi_utils.midi_note_nr_to_name(self.note)) + '{:>30}'.format(str(self.duration)) + '{:>30}'.format(str(self.delay)) + '{:>15}'.format(str(self.velocity))
