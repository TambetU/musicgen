note_names = [
    "C",
    "C#",
    "D",
    "D#",
    "E",
    "F",
    "F#",
    "G",
    "G#",
    "A",
    "A#",
    "H",
    ]

def midi_note_nr_to_name(note_nr):
    n = note_nr%12
    return note_names[n]