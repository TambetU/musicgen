import keras.backend as K
import tensorflow as tf
import os
import mido
import random
import configparser

from src.midi_output import MidiOutput
from src.midi_input import MidiInput
from src.random_song import gen

from src import midi_converter, bars
from models import combined_model_prediction as combined_model, note_model_prediction as note_model, \
    bar_prediction as bar_model



tf.logging.set_verbosity(tf.logging.ERROR)
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

parser = configparser.ConfigParser()
config = parser.read("./config.py")

config = configparser.ConfigParser()
config.read('config.ini')
config = config["DEFAULT"]


def set_use_cpu():
    num_CPU = 1
    num_GPU = 0
    num_cores = 4

    config = tf.ConfigProto(
                            intra_op_parallelism_threads=num_cores, \
                            inter_op_parallelism_threads=num_cores, allow_soft_placement=True, \
                            device_count={'CPU': num_CPU, 'GPU': num_GPU})

    session = tf.Session(config=config)
    K.set_session(session)

def save_midi(midi):
    m = MidiOutput(0)
    if config.getboolean("save_after") == True:
        m.save_to_file(midi, config["save_file"])
        print("Midi salvestatud 'gen.mid' faili.")

def play_midi(midi, dbnn, seq_length, extend):
    m = MidiOutput(0)
    rtmidi = mido.Backend('mido.backends.rtmidi')
    i = 0
    for msg in midi.play():
        if msg.type == "note_on":
            print(dbnn.notes[i])
            if i == seq_length and extend:
                print("GENEREERITUD: ")
            i += 1

        m.send_to_output(msg)

def ask_for_file(q, dir, type = "a_model_weights", model_type = 0):
    print("")
    files = [os.path.join(dir, f) for f in os.listdir(dir) if os.path.isfile(os.path.join(dir, f))]

    if type == "a_model_weights":
        if model_type == 0:
            ser = "combined"
        else:
            ser = "note"
    elif type == "midi":
        ser = ".mid"
    else:
        ser = ""

    files = [f for f in files if ser in f]

    print("Valikus failid:")
    for i in range(0, len(files), 2):
        if len(files) > i + 1:
            print("{: <40} {: <40}".format(str(i) + files[i], str(i+1) + files[i+1]))
        else:
            print("{: <40}".format(str(i) + files[i]))

    try:
        inp = input(q)
        if inp == "":
            return files[random.randint(0, len(files) - 1)]

        input_model = int(inp)

    except ValueError:
        return ask_for_file(dir, type, model_type)

    if input_model in range(len(files)):
        return files[input_model]
    else:
        return ask_for_file(dir, type, model_type)

def ask_input(q, possible_answers, nr = False):
    print("")
    try:
        inp = input(q)
        if inp == "":
            if nr:
                return [0,2][random.randint(0,1)] #kui random, et ei tuleks midi sisendit
            else:
                return possible_answers[random.randint(0, len(possible_answers) - 1)]
        answer = int(inp)
    except ValueError:
        return ask_input(q, possible_answers)

    if answer in possible_answers:
        return answer
    else:
        return ask_input(q, possible_answers)

def ask_for_number(q):
    print("")
    try:
        inp = input(q)
        if inp == "":
            return random.randint(30, 500)
        answer = int(inp)
    except ValueError:
        return ask_for_number(q)

    if answer > 0:
        return answer
    else:
        return ask_for_number(q)

def ask_for_temp(q):
    print("")
    try:
        inp = input(q)
        if inp == "":
            return random.uniform(0.1,3)
        answer = float(inp)
    except ValueError:
        return ask_for_temp

    if answer > 0:
        return answer
    else:
        return ask_for_temp




set_use_cpu()

use_cli = config.getboolean("cli")
model_type = config.getint("model_type")
seed_type = config.getint("seed_type")

sequence_length = config.getint("sequence_length")
extend = config.getboolean("extend")
temperature =config.getfloat("temperature")

amount = config.getint("amount")
seed_file = config.get("seed_file")
tempo = config.getint("tempo")

seed_bar_vec =  config.getint("seed_bar_vec")
bar_vecs_file = config.get("bar_vecs_file")
bar_length = config.getfloat("bar_length")

weights_p = config.get("weights_p")
weights_l = config.get("weights_l")
weights_bar = config.get("weights_bar")

midi_dir = config.get("midi_dir")
weights_dir = config.get("weights_dir")
bar_dir = config.get("bar_dir")

play_after = config.getboolean("play_after")
seed_random_start = config.getboolean("seed_random_start")

print("\n" * 100)
if use_cli:
    print("Sisestamata väärtuste puhul valitakse sisend juhuslikult.")
    model_type = ask_input("Kumb mudel: kombineeritud (0) või noodipõhine (1)?:", [0, 1])
    print("Valisid:", model_type)


    weights_file = ask_for_file("Vali mudeli kaalude fail:", weights_dir, type="a_model_weights", model_type = model_type)
    print("Valisid: ", weights_file)
    weights_p = weights_file
    weights_l = weights_file

    seed_type = ask_input("Milline sisend, mille põhjal genereerida: MIDI fail (0), MIDI sisendseadmest (1) või puudub(2)?:", [0,1,2], True)
    print("Valisid: ", seed_type)

    sequence_length = ask_for_number("Kui pikkade jadade põhjal noote ennustada?: ")
    print("Valisid: ", sequence_length)

    if seed_type == 0:
        seed_file = ask_for_file("Vali näidis midi fail: ", midi_dir, type = "midi")
        print("Valisid: ", seed_file)

    extend = ask_input("Kas genereeritud muusika ette lisada ka näidis? ei(0) / jah(1)", [0, 1])
    print("Valisid: ", extend)

    seed_bar_vec = ask_input("Kuidas luua taktivektorid: näidise omad (0), genereerida (1) või muust failist (2)?", [0,1,2])
    print("Valisid: ", seed_bar_vec)

    if seed_bar_vec == 1:
        weights_bar = ask_for_file("Vali taktivektori mudeli kaalude fail: ", bar_dir, type = "bar")
        print("Valisid: ", weights_bar)
    if seed_bar_vec == 2:
        bar_vecs_file = ask_for_file("Vali midi fail, mille põhjal luuakse taktivektorid: ", midi_dir, type="midi")
        print("Valisid: ", bar_vecs_file)

    amount = ask_for_number("Mitme noodi pikkune jupp genereerida?: ")
    print("Valisid: ", amount)

    temperature = ask_for_temp("Sisesta temp (>0, 1 = argmax, näiteks 0.5):")
    print("Valisid: ", temperature)






#Kui midi sisendseadmest
if seed_type == 1:

    midi_in = MidiInput()
    sequence_length = 35 # >= 35

    print("Alusta MIDI seadmel mängimist (jälgi tempot): ")
    midi = midi_in.listen(tempo, amount=sequence_length)
    dbnn = midi_converter.dbnn_from_midi_object(midi)[0]

#Sisendfail
elif seed_type == 0:
    dbnn = midi_converter.file_to_dbnn(seed_file)
    if dbnn == None:
        print("Faili ei õnnestunud lugeda")
        exit()
    dbnn = sorted(dbnn,reverse = True, key = lambda x: len(x.notes))
    dbnn = dbnn[0]


    if seed_random_start:
        dbnn.notes = dbnn.notes[random.randint(0, len(dbnn.notes) - sequence_length - 1):]

#Pole sisendit
else:
    dbnn = gen(5)


if seed_bar_vec == 0:
    bar_vecs = bars.bar_notes_to_bars(bars.divide_dbnn_into_bars(dbnn), dbnn)
    for i in range(5):
        bar_vecs.extend(bar_vecs)

elif seed_bar_vec == 1:
    bar_vecs = bar_model.generate(300,weights_bar, dbnn)

elif seed_bar_vec == 2:
    bar_dbnn = midi_converter.file_to_dbnn(bar_vecs_file)
    bar_vecs = bars.bar_notes_to_bars(bars.divide_dbnn_into_bars(dbnn), dbnn)
    for i in range(5):
        bar_vecs.extend(bar_vecs)

if seed_bar_vec == 0:
    pass
else:
    #generate bars
    pass


if model_type == 0:
    dbnn = combined_model.generate(sequence_length, weights_p, dbnn, bar_vecs = bar_vecs, bar_length= bar_length, amount = amount, temperature = temperature, extend = extend, tempo = tempo)
else:
    dbnn = note_model.generate(sequence_length, weights_l, dbnn, bar_vecs=bar_vecs, amount=amount, temperature=temperature,
                                   extend=extend, tempo = tempo)


midi = midi_converter.dbnn_to_midi_object(dbnn)
save_midi(midi)
if play_after:
    play_midi(midi,dbnn, sequence_length, extend)









